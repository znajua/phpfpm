FROM php:7.1-fpm
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng12-dev \
        ssmtp \
        libxml2-dev \
        libz-dev libmemcached-dev \
    && docker-php-ext-install -j$(nproc) iconv mcrypt \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && rm -rf /var/lib/apt/lists/*;

RUN docker-php-ext-install pdo && docker-php-ext-enable pdo \
&& docker-php-ext-install pdo_mysql && docker-php-ext-enable pdo_mysql \
&& docker-php-ext-install session && docker-php-ext-enable session \
&& docker-php-ext-install mbstring && docker-php-ext-enable mbstring \
&& docker-php-ext-install mysqli && docker-php-ext-enable mysqli \
&& docker-php-ext-install soap && docker-php-ext-enable soap \
&& docker-php-ext-install zip && docker-php-ext-enable zip \
&& docker-php-ext-install opcache && docker-php-ext-enable opcache

RUN pecl install -o -f redis && rm -rf /tmp/pear \
&&  docker-php-ext-enable redis

RUN pecl install memcached && rm -rf /tmp/pear \
&& docker-php-ext-enable memcached

RUN apt-get update && apt-get install -y libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN pecl install imagick && docker-php-ext-enable imagick


# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

RUN sed -i -e "s/;security.limit_extensions = .php .php3 .php4 .php5 .php7/security.limit_extensions = /g" /usr/local/etc/php-fpm.d/www.conf

#
